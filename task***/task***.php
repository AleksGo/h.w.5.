<?php
function dd($arr)
{
    echo "<pre style='display:inline-block;'>";
    print_r($arr);
    echo "</pre>";
}

function df($MySTR, $FILE = 'MyLog.txt')
{
    $date = date("Y-m-d G:i:s");
    $fp = fopen($_SERVER["DOCUMENT_ROOT"] . $FILE, 'a+');
    $str = $date . " " . print_r($MySTR, true) . "\r\n";
    fwrite($fp, $str);
    fclose($fp);
}

// 1) Дан массив, Развернуть этот массив в обратном направлении
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
echo "<b> 1) Дан массив, Развернуть этот массив в обратном направлении ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']</b><br>";
$namesArr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
// 1 - for
$lengthArr = 0;
for ($i = 0; isset($namesArr[$i]); $i++) {
    $lengthArr = $i;
}
for ($i = $lengthArr; $i >= 0; $i--) {
    $namesArrReverseFor[] = $namesArr[$i];
}
dd($namesArrReverseFor);
// 1 - while
$i = 0;
while (isset($namesArr[$i])) {
    $i++;
}
$i = $lengthArr;
while ($i >= 0) {
    $namesArrReverseWhile[] = $namesArr[$i];
    $i--;
}
dd($namesArrReverseWhile);
// 1 - do-while
$i = 0;
do {
    $lengthArr = $i;
    $i++;
} while (isset($namesArr[$i]));
$i = $lengthArr;
do {
    $namesArrReverseDoWhile[] = $namesArr[$i];
    $i--;
} while ($i >= 0);
dd($namesArrReverseDoWhile);
// 1 - foreach
$lengthArr = -1;
foreach ($namesArr as $name) {
    $lengthArr++;
}
foreach ($namesArr as $name) {
    $namesArrReverseForeach[] = $namesArr[$lengthArr];
    $lengthArr--;
}
dd($namesArrReverseForeach);

// 2) Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69] Развернуть этот массив в обратном направлении
echo "<br><hr><b> 2) Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69] Развернуть этот массив в обратном направлении</b><br>";
$numberArr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
// 2 - for
$lengthArr = 0;
for ($i = 0; isset($numberArr[$i]); $i++) {
    $lengthArr = $i;
}
for ($i = $lengthArr; $i >= 0; $i--) {
    $numberArrReverseFor[] = $numberArr[$i];
}
dd($numberArrReverseFor);
// 2 - while
$i = 0;
while (isset($numberArr[$i])) {
    $i++;
}
$i = $lengthArr;
while ($i >= 0) {
    $numberArrReverseWhile[] = $numberArr[$i];
    $i--;
}
dd($numberArrReverseWhile);
// 2 - do-while
$i = 0;
do {
    $lengthArr = $i;
    $i++;
} while (isset($numberArr[$i]));
$i = $lengthArr;
do {
    $numberArrReverseDoWhile[] = $numberArr[$i];
    $i--;
} while ($i >= 0);
dd($numberArrReverseDoWhile);
// 3 - foreach
$lengthArr = -1;
foreach ($numberArr as $number) {
    $lengthArr++;
}
foreach ($numberArr as $number) {
    $numberArrReverseForeach[] = $numberArr[$lengthArr];
    $lengthArr--;
}
dd($numberArrReverseForeach);

// 3) Дана строка let str = 'Hi I am ALex' развенуть строку в обратном направлении.
echo "<br><hr><b> 3) Дана строка let str = 'Hi I am ALex' развенуть строку в обратном направлении.</b><br>";
$str = 'Hi I am ALex';
// 3 - for
$lengthArr = 0;
for ($i = 0; isset($str[$i]); $i++) {
    $lengthArr = $i;
}
for ($i = $lengthArr; $i >= 0; $i--) {
    $strReverseFor .= $str[$i];
}
echo $strReverseFor, "<br>";
// 3 - while
$i = 0;
while (isset($str[$i])) {
    $i++;
}
$i = $lengthArr;
while ($i >= 0) {
    $strReverseWhile .= $str[$i];
    $i--;
}
echo $strReverseWhile, "<br>";
// 3 - do-while
$i = 0;
do {
    $lengthArr = $i;
    $i++;
} while (isset($str[$i]));
$i = $lengthArr;
do {
    $strReverseDoWhile .= $str[$i];
    $i--;
} while ($i >= 0);
echo $strReverseDoWhile, "<br>";
// 3 - foreach
for ($i = 0; isset($str[$i]); $i++) {
    $strArr[] = $str[$i];
}
$lengthArr = -1;
foreach ($strArr as $words) {
    $lengthArr++;
}
foreach ($strArr as $words) {
    $strReverseForeach .= $strArr[$lengthArr];
    $lengthArr--;
}
echo $strReverseForeach, "<br>";

//4) Дана строка. готовую функцию toUpperCase() or tolowercase()
//let str = 'Hi I am ALex'
//сделать ее с с маленьких букв
echo "<br><hr><b>4) Дана строка. готовую функцию toUpperCase() or tolowercase() let str = 'Hi I am ALex' сделать ее с с маленьких букв</b><br>";
$str = 'Hi I am ALex';
// 4 - for
for ($i = 0; isset($str[$i]); $i++) {
    $strLowerFor .= strtolower($str[$i]);
}
echo $strLowerFor, "<br>";
// 4 - while
$i = 0;
while (isset($str[$i])) {
    $strLowerWhile .= strtolower($str[$i]);
    $i++;
}
echo $strLowerWhile, "<br>";
// 4 - do-while
$i = 0;
do {
    $strLowerDoWhile .= strtolower($str[$i]);
    $i++;
} while (isset($str[$i]));
echo $strLowerDoWhile, "<br>";
// 4 - foreach
$str = 'Hi I am ALex';
for ($i = 0; isset($str[$i]); $i++) {
    $strArr[$i] = $str[$i];
}
foreach ($strArr as $words) {
    $strLowerForeach .= strtolower($words);
}
echo $strLowerForeach;
// 5) Дана строка
//let str = 'Hi I am ALex'
//сделать все буквы большие
echo "<br><hr><b>5) Дана строка let str = 'Hi I am ALex'сделать все буквы большие</b><br>";
// 5 - for
for ($i = 0; isset($str[$i]); $i++) {
    $strUpperFor .= strtoupper($str[$i]);
}
echo $strUpperFor, "<br>";
// 4 - while
$i = 0;
while (isset($str[$i])) {
    $strUpperWhile .= strtoupper($str[$i]);
    $i++;
}
echo $strUpperWhile, "<br>";
// 4 - do-while
$i = 0;
do {
    $strUpperDoWhile .= strtoupper($str[$i]);
    $i++;
} while (isset($str[$i]));
echo $strUpperDoWhile, "<br>";
// 4 - foreach
for ($i = 0; isset($str[$i]); $i++) {
    $strArr[$i] = $str[$i];
}
foreach ($strArr as $words) {
    $strUpperForeach .= strtoupper($words);
}
echo $strUpperForeach;
// 7) Дан массив
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//сделать все буквы с маленькой
$namesArr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
echo "<br><hr><b>7) Дан массив['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']сделать все буквы с маленькой</b><br>";
// 7 - for
for ($i = 0; isset($namesArr[$i]); $i++) {
    $namesInArr = $namesArr[$i];
    for ($k = 0; isset($namesInArr[$k]); $k++) {
        $namesInArrLowerFor .= strtolower($namesInArr[$k]);
    }
    $namesArr[$i] = $namesInArrLowerFor;
    $namesInArrLowerFor = null;
}
dd($namesArr);
// 7 - while
$namesArr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$i = 0;
while (isset($namesArr[$i])) {
    $namesInArr = $namesArr[$i];
    $k = 0;
    while (isset($namesInArr[$k])) {
        $namesInArrLowerWhile .= strtolower($namesInArr[$k]);
        $k++;
    }
    $namesArr[$i] = $namesInArrLowerWhile;
    $namesInArrLowerWhile = null;
    $i++;
}
dd($namesArr);
// 7 - do-while
$namesArr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$i = 0;
do {
    $namesInArr = $namesArr[$i];
    $k = 0;
    do {
        $namesInArrLowerDoWhile .= strtolower($namesInArr[$k]);
        $k++;
    } while (isset($namesInArr[$k]));
    $namesArr[$i] = $namesInArrLowerDoWhile;
    $namesInArrLowerDoWhile = null;
    $i++;
} while (isset($namesArr[$i]));
dd($namesArr);
// 7 - foreach
$namesArr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$i = 0;
foreach ($namesArr as $names) {
    $namesArr[$i] = strtolower($names);
    $i++;
}
dd($namesArr);
//8) Дан массив
//['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
//сделать все буквы с большой
echo "<br><hr><b>8) Дан массив['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']сделать все буквы с большой</b><br>";
// 8 - for
for ($i = 0; isset($namesArr[$i]); $i++) {
    $namesInArr = $namesArr[$i];
    for ($k = 0; isset($namesInArr[$k]); $k++) {
        $namesInArrUpperFor .= strtoupper($namesInArr[$k]);
    }
    $namesArr[$i] = $namesInArrUpperFor;
    $namesInArrUpperFor = null;
}
dd($namesArr);
// 9 - while
$namesArr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$i = 0;
while (isset($namesArr[$i])) {
    $namesInArr = $namesArr[$i];
    $k = 0;
    while (isset($namesInArr[$k])) {
        $namesInArrUpperWhile .= strtoupper($namesInArr[$k]);
        $k++;
    }
    $namesArr[$i] = $namesInArrUpperWhile;
    $namesInArrUpperWhile = null;
    $i++;
}
dd($namesArr);
// 8 - do-while
$namesArr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$i = 0;
do {
    $namesInArr = $namesArr[$i];
    $k = 0;
    do {
        $namesInArrUpperDoWhile .= strtoupper($namesInArr[$k]);
        $k++;
    } while (isset($namesInArr[$k]));
    $namesArr[$i] = $namesInArrUpperDoWhile;
    $namesInArrUpperDoWhile = null;
    $i++;
} while (isset($namesArr[$i]));
dd($namesArr);
// 7 - foreach
$namesArr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$i = 0;
foreach ($namesArr as $names) {
    $namesArr[$i] = strtoupper($names);
    $i++;
}
dd($namesArr);
// 9) Дано число
//let num = 1234678
//развернуть ее в обратном направлении
echo "<br><hr><b>9) Дано число let num = 1234678 развернуть ее в обратном направлении</b><br>";
$num = 1234678;
$strNum = (string)$num;
// 9 - for
$lengthArr = 0;
for ($i = 0; isset($strNum[$i]); $i++) {
    $lengthArr = $i;
}
for ($i = $lengthArr; $i >= 0; $i--) {
    $numReverseFor .= $strNum[$i];
}
$numReverseFor = (int)$numReverseFor;
echo $numReverseFor, "<br>";
// 9 - while
$i = 0;
while (isset($strNum[$i])) {
    $i++;
}
$i = $lengthArr;
while ($i >= 0) {
    $numReverseWhile .= $strNum[$i];
    $i--;
}
$numReverseWhile = (int)$numReverseWhile;
echo $numReverseWhile, "<br>";
// 9 - do-while
$i = 0;
do {
    $lengthArr = $i;
    $i++;
} while (isset($strNum[$i]));
$i = $lengthArr;
do {
    $strNumReverseDoWhile .= $strNum[$i];
    $i--;
} while ($i >= 0);
$strNumReverseDoWhile = (int)$strNumReverseDoWhile;
echo $strNumReverseDoWhile, "<br>";
// 9 - foreach
for ($i = 0; isset($strNum[$i]); $i++) {
    $strNumArr[] = $strNum[$i];
}
$lengthArr = -1;
foreach ($strNumArr as $words) {
    $lengthArr++;
}
foreach ($strNumArr as $words) {
    $strNumReverseForeach .= $strNumArr[$lengthArr];
    $lengthArr--;
}
$strNumReverseForeach = (int)$strNumReverseForeach;
echo $strNumReverseForeach, "<br>";
// 10) Дан массив
//[44, 12, 11, 7, 1, 99, 43, 5, 69]
//отсортируй его в порядке убывания
echo "<br><hr><b>10) Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69] отсортируй его в порядке убывания</b><br>";
$arrNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$lengthArr = 0;
for ($i = 0; isset($arrNumber[$i]); $i++) {
    $lengthArr = $i;
}
// 10 - for
for ($k = 0; $k <= $lengthArr; $k++) {
    for ($i = 1; $i <= $lengthArr; $i++) {
        $y = $i - 1;
        if ($arrNumber[$y] < $arrNumber[$i]) {
            $x = $arrNumber[$i];
            $arrNumber[$i] = $arrNumber[$y];
            $arrNumber[$y] = $x;
        }
    }
}
dd($arrNumber);
// 10 - while
$arrNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$k = 0;
while ($k <= $lengthArr) {
    $i = 1;
    while ($i <= $lengthArr) {
        $y = $i - 1;
        if ($arrNumber[$y] < $arrNumber[$i]) {
            $x = $arrNumber[$i];
            $arrNumber[$i] = $arrNumber[$y];
            $arrNumber[$y] = $x;
        }
        $i++;
    }
    $k++;
}
dd($arrNumber);
// 10 - do-while
$arrNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
$k = 0;
do {
    $i = 1;
    do {
        $y = $i - 1;
        if ($arrNumber[$y] < $arrNumber[$i]) {
            $x = $arrNumber[$i];
            $arrNumber[$i] = $arrNumber[$y];
            $arrNumber[$y] = $x;
        }
        $i++;
    } while ($i <= $lengthArr);
    $k++;
} while ($k <= $lengthArr);
dd($arrNumber);
// 10 - foreach
$arrNumber = [44, 12, 11, 7, 1, 99, 43, 5, 69];
for ($i = 0; isset($arrNumber[$i]); $i++) {
    foreach ($arrNumber as $key => $num) {
        if ($key === 0) {
            $x = $num;
            continue;
        } elseif ($num > $x) {
            $arrNumber[$key - 1] = $num;
            $arrNumber[$key] = $x;
        } else {
            $x = $num;
        }
    }
}
dd($arrNumber);
?>