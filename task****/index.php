<?php

include 'form.php';

if (isset($_GET['guess']) && $_GET['guess'] !== '') {
    $playerNumber = $_GET['guess'];
    $randNumber = rand(5, 8);
    if ($playerNumber < 5) {
        echo 'Число маленькое';
    } elseif ($playerNumber > 8) {
        echo 'Число большое';
    } elseif ($playerNumber == $randNumber) {
        echo '<p>Вы угадали! <a href="index.php">Играть еще!</a></p>';
    } else {
        echo "<p>Вы близко к правильному ответу, <a href='index.php'>Попробовать еще!!!</a></p>";
    }
};

?>